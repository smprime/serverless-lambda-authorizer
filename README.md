# Welcome to serverless-lambda-authorizer 👋
![Version](https://img.shields.io/badge/version-1.0.1-blue.svg?cacheSeconds=2592000)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](#)
[![Twitter: smprime](https://img.shields.io/twitter/follow/smprime.svg?style=social)](https://twitter.com/smprime)

> serverless lambda authorizer

### 🏠 [Homepage](https://bitbucket.org/smprime/serverless-lambda-authorizer#readme)

## Author

👤 **Jason Mullings**

* Website: https://mysm.com/
* Twitter: [@smprime](https://twitter.com/smprime)
* Github: [@smprime](https://github.com/smprime)

## Show your support

Give a ⭐️ if this project helped you!


***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_