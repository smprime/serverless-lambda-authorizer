'use strict';

class ServerlessXLambdaAuthorizerPlugin {
  constructor(serverless, options) {
    this.serverless = serverless;
    const naming = this.serverless.providers.aws.naming;

    this.getMethodLogicalId = naming.getMethodLogicalId.bind(naming);
    this.normalizePath = naming.normalizePath.bind(naming);
    this.options = options;

    this.hooks = {
      'before:package:finalize': this.applyPlugin.bind(this)
    };
  }

  applyPlugin() {
    const resources = this.serverless.service.provider
      .compiledCloudFormationTemplate.Resources;

    this.applyAuthorizers(resources).catch(e => {
      this.serverless.cli.log('Could not apply cross lambda Authorizers');
    });
  }

  applyAuthorizers(resources) {
    try {
      if ('xLambdaAuthorizers' in this.serverless.service.custom) {
        const authorizers = this.serverless.service.custom[
          'xLambdaAuthorizers'
        ];

        authorizers.forEach(authorizer => {
          resources[authorizer.Name] = {
            Type: 'AWS::ApiGateway::Authorizer',
            Properties: {
              ...authorizer,
              RestApiId: {
                Ref: 'ApiGatewayRestApi'
              }
            }
          };
        });

        this.serverless.service.getAllFunctions().forEach(functionName => {
          const functionObject = this.serverless.service.functions[
            functionName
          ];

          functionObject.events.forEach(event => {
            if (!event.http) {
              return;
            }
            if ('xLambdaAuthorizer' in event.http) {
              const path = event.http.path;
              const method = event.http.method;

              const resourcesArray = path.split('/');
              // resource name is the last element in the endpoint. It's not unique.
              const resourceName = path.split('/')[path.split('/').length - 1];
              const normalizedResourceName = resourcesArray
                .map(this.normalizePath)
                .join('');
              const normalizedMethod =
                method[0].toUpperCase() + method.substr(1).toLowerCase();
              const methodName = `ApiGatewayMethod${normalizedResourceName}${normalizedMethod}`;
              this.serverless.cli.log(
                `Applying ${event.http.xLambdaAuthorizer} to ${methodName}`
              );
              resources[methodName].Properties['AuthorizerId'] = {
                Ref: event.http.xLambdaAuthorizer
              };
              resources[methodName].Properties['AuthorizationType'] = 'CUSTOM';
              resources[methodName]['DependsOn'] = event.http.xLambdaAuthorizer;
            }
          });
        });
        
      } else {
        this.serverless.cli.log(
          'Could not find custom variable xLambdaAuthorizers'
        );
        return Promise.reject();
      }

      return Promise.resolve();
    } catch (e) {
      return Promise.reject();
    }
  }
}

module.exports = ServerlessXLambdaAuthorizerPlugin;
